## Why my GitHub account really got terminated

I need to write this otherwise people would make things up and spread misinformation (_like they did in the past_).


## What happened?
I got reported and as a result GitHub flagged my account (_meaning my content is hidden from the public but you still get limited access_). After opening a [support ticket](https://support.github.com/ticket/personal/0/815409) which had after 2 days invisible conversations on it and waiting for over 20 days, GitHub finally decided to take action. After 15 days or so I created other tickets to bump my original ticket because there is no reply button. You only can reply via eMail which is strange (so you cannot edit or take control over you already posted content). I did this because people continuously asked me what happened and I could not answer them because I also did not knew what really was going on.


## GitHub mentioned 2 reasons

```
- violating our policy against repeated infringement.
- violating our policy against content that is abusive toward any individual or group.
```

It's **NOT** related to [DMCA](https://docs.github.com/en/free-pro-team@latest/github/site-policy/guide-to-submitting-a-dmca-counter-notice), the last [DMCA](https://github.com/contact/dmca) request I won and GitHub had to remove the false claim. That was with the [HWIDGEN lawsuit](https://github.com/CHEF-KOCH/HWIDGEN-SRC/issues). I know people want this the main reason but GitHub staff mentioned it without explaining why and how this is related to my flagged account or the issue ticket. The original flagging was because of ONE single pull requests which is nothing but laughable because cheaters simply provoke me (and many others). I see DMCA in general as a tool to troll and to get rid of your competition, which is widely abused by lots of people to block you or your projects. You find a lot of [troll take down requests](https://github.com/github/dmca/pull/7417) easily. I asked GitHub to point me out what rule I specifically broke and even pointed our that a [DMCA](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act) request does not make you look guilty, this point entirely gets ignored. There is and never will be any evidence, if there would be people would already sue me but this is not the case, besides I did provided evidence for all my counter notices and GitHub is fully aware of it.


So again, I got reported because [ONE pull request](https://github.com/sagirilover/AnimeSoftware/pull/15/commits/1e92dc2851c71da8df0ecb19089c1559fbf31d26) (I talked with a GitHub Staff on Discord and the said only ONE of the mentioned pull request got reported over to GitHub!) with `"I 'm a fucking cheater and have a small penis"`. Which expresses my disrespect for cheater in general and to make a statement/point that GitHub should not be abused by cheater to spread their code. The owner btw obfuscate his project with a fake project name: `Anime Software` without mention or tagging the word "cheat" or implying that this repo is for game cheats (promotion/selling) purposes. This practice is against [GitHub Community Guidelines](https://docs.github.com/en/free-pro-team@latest/github/site-policy/github-community-guidelines) since the Title spreads misinformation regarding the real purpose of this project. The Readme.md files are differently been indexed compared to normal tags or the project overview/description.

I admit that my action was overall not okay but I think this does not deserve an account ban. I saw other things on GitHub and those individuals got away with it. If you host cheats you simply need to deal with criticism and disrespect, this should be clear, people might respond and react. How someone can feel offended and that GitHub even accept this as reason for "abuse" is beyond me. These people provoke and cause damage (_please see for more details below_).


## My expression
- I do not tolerate cheaters in general, I never have and I never will - we're talking about online cheats e.g. for CS:GO, if you cheat in private for yourself without "hurting" or offending someone this is a different story.
- I can express my feelings and opinion towards others freely and also their projects. GitHub sees this part different but it's my right (people also did this towards me and my projects).
- In some of my month olds pull request I used the _f word_ which was not okay and I agree this is wrong but after getting 300 eMails a day with "pls do something against cheater" you maybe freak out one day, like I did because it annoys me as hell that GitHub allows to host cheat code + to get such eMails. The thing is that such things makes it harder for all of us (gamers). The code is also not for "educational purposes" since those individuals want to gain money out of it or their motivation is that others take the code and fix it to make it "working" again (_meaning bypassing anti-cheat systems like VAC).
- I do not think that one comment/pull request is cyberbullying or harassment, especially not because GitHub allows you to block others. This normal procedure is explained over [here](https://docs.github.com/en/free-pro-team@latest/github/site-policy/github-community-guidelines#what-if-something-or-someone-offends-you).

Their overall reason was that I "multiple times" _sabotaged_/abused pull requests to manipulate something which is against GitHub as well as every game ToS. Well, what can I say I do not see any reason that GitHub or anyone else should tolerate it. I got ONCE (singular) reported, the other mentioned pull requests just blocked me, which is okay and I would have done exactly the same.


## Is creating and selling “cheats” or “hacks” for games illegal?

The topic as well as the title itself is covered in full [here](https://law.stackexchange.com/questions/25825/is-creating-and-selling-cheats-or-hacks-for-games-illegal). The short answer is **YES**. 

We have the following:
- [17 U.S. Code § 106 - Exclusive rights in copyrighted works](https://www.law.cornell.edu/uscode/text/17/106), which is defined and explained over [here](https://www.law.cornell.edu/uscode/text/17/101).
- [17 U.S. Code § 501](https://www.law.cornell.edu/uscode/text/17/501)
- [17 U.S. Code § 506 - Criminal offenses](https://www.law.cornell.edu/uscode/text/17/506)
- [18 U.S. Code § 2319](https://www.law.cornell.edu/uscode/text/18/2319)

In short: Such cheat people on purpose bypassing the game/service ToS and GitHub is guitly too since they deliberately and fully ware of these circumstances host such material. 

## Info regarding cheating in games
[Even BBC news reported about cheater](https://www.youtube.com/watch?v=gZKFmW9l1ic) and the term `fucking cheater` seems to be allowed and tolerated on YouTube. I also do not see how this is abuse, if you do not cheat or in case you're not affected then there is no point in getting _offended_. If you do, you should ask yourself what damage you do to others and what it means to abuse exploits to get an unfair benefit in-game + the consequences for others. This is mainly the reason why there exist anti-cheat counter measurements.

Fun fact, there a [videos which is over 9 years old](https://www.youtube.com/watch?v=BYkzMDffjUE) hosted on YouTube with "fucking cheater" as title + description, pointing directly to cheaters and one single induvidual. There are multiple others pointing at individuals calling them out and YouTube allows it.

However, on the other side most sellers using [YouTube as selling platform](https://www.youtube.com/results?search_query=selling+cs%3Ago+cheat+github.com) and [GitHub as hosting platform](https://www.google.com/search?newwindow=1&safe=strict&q=github.com+cheats) and/or Discord as communication platform. There are even [guides how to sell cheats](https://www.youtube.com/watch?v=I6xBbITYedo). YouTube seems to ignore the reports or comments, and the comments are often much worse (some of them automatically getting deleted by YouTube).

I think calling out cheater and pointing at them is more than okay, they make money and trying to sell their crap and you want to stop that. So there is a normal "relationship" between these two things.



## Real Reason?
I have the impression that GitHub staff was biased and not objective as he should be. Given the fact that MS lost a lawsuit against me + GitHub faces an upcoming lawsuit for not giving users their entire data back might explain their real motivation for their wrong decision of my account termination.

GitHub staff overall came up with something unrelated and totally ignored the fact that they refuse to forwarded my [counter notice](https://support.github.com/contact/dmca-counter-notice), this alone made them looks biased (_for whatever reason_). Btw once you counter notice an DMCA request the one you created it has to sue you otherwise GitHub takes the request down after [10-14 days](https://docs.github.com/en/free-pro-team@latest/github/site-policy/dmca-takedown-policy). No one ever went into a lawsuit with me (except Microsoft, and they lost).

It's more than shady from GitHub to terminate my account based on something which is irrelevant (and not proven) and only one single incident because I (and others) getting provoked by these cheat developer. This could have been prevented if GitHub does not allow cheat code in general.



## Conclusion
Cheaters seems to be allowed/wanted by GitHub and this results in making the entire gaming situation for all of us much worse. This is unacceptable and I stand by my word that cheaters have a small penis because why would you do it otherwise? Right, to compensate. There is also a common [study about why people with small penises cheating](https://www.bustle.com/articles/22778-theres-a-correlation-between-penis-size-and-cheatingbut-its-highly-suspect), so they come back online and want to get their frustration out of their body. Which might explain why someone cheats in the first place.

**The problem** is that cheaters can now use this and continue to provoke others, while they have nothing to fear because GitHub only take actions once you report someone. This is wrong and something which causes damage (also financial) and should not be tolerated at all. The decision to ban be while others started to violating the Community Guidelines as well as ToS first is beyond me. I definitely overreacted but it's maybe understandable after getting 300 emails a day with "pls do something against these cheaters" - I already said this but do it again so that people understand that this is frustrating and wasted my time - and at some points it's enough and you have to get things out of your body. I do not say that I should have been used the F word (in one unrelated mentioned pull request) but then again provoking all of us normal gamers with such things is not easy to look away from. [Twitch for example terminates your account if you get busted cheating](https://www.ginx.tv/en/call-of-duty/twitch-streamer-accidentally-shows-he-s-cheating-and-gets-channel-deleted), which is fine.

**I urge GitHub to ban cheaters and their cheat code** because it causes massive problems, the games getting more expensive because they need to implement lots of effort and anti-cheat mechanism to make the games usable, again this results in more expensive games for all of us and the developer power could have been used for useful things. Besides the monitoring for anti-cheat implementations in game clients can also create privacy concerns and slow-downs in-game or the entire system if it scans and submits unknown amount of data.

Another problem is that GitHub do not allow any slight criticism on their service/platform (see their ToS) and they simply remove every content they do not like or disagree with. This already happened and they will continue their shady practice without that you get notified or be able to respond/justify your content.

GitHub is fundamentally flawed and influenced by people who only care about money, they prioritize "Pro" users over others. Overall they give no shits about old users like me or their opinion but on the other side they write shit like "be open minded if you go into discussions", this is double speech and makes the entire story even funnier.


**Give misinformation, false accusations and cheaters NO CHANCE.**

Greetings,

CK

**Status**
- 27.08.2020 - GitHub support ticket #815409 created, because my account got [flagged](https://github.community/t/my-account-is-flagged/282)
- 18.08.2020 - Created other tickets (e.g. 827035) to bump my original ticket because a lack of response
- 18 - 27.09 - I posted multiple tweets regarding this directly to GitHub without getting any response
- 27.09.2020 - GitHub already deleted pull requests, which makes it even less understandable why this is still hold against me and result in an account termination
- 28.09.2020 - Tried to add an [successor](https://docs.github.com/github/setting-up-and-managing-your-github-user-account/maintaining-ownership-continuity-of-your-user-accounts-repositories) which failed because my Account is flagged, after GitHub advertised that I should do that 
- 29.09.2020 - GitHub Staff responded once again claiming that some of my repositories are been disabled due to DMCA, this is correct but once again they ignore that I did send a counter notice without ever getting a confirmation/response back from GitHub. They entirely ignore that they have to respond to every single counter notice (_which they did not_). Once again there is also no explanation why this is related to the "abuse" report. 
- 30.09.2020 - I responded one last time to GitHub Staff expressing that they simply ignore some important parts in their story and that I was under the impression that they just screened my almost 10 years old account to find something to get rid of me, ignoring that I responded to everything. I also mentioned that I cannot export my account or assign an successor (due to an internal error - a screenshot was uploaded in the "successor" folder).
- 04.10.2020 - My account is still flagged and not deleted, GitHub did not answered.
